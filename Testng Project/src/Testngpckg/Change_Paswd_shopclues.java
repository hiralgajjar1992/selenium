package Testngpckg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;


import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
public class Change_Paswd_shopclues {
	public WebDriver driver;
	
	@BeforeTest
	@Parameters("browser")
	  public void beforeTest(@Optional("firefox")String browser) throws Exception {
		//System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
	   // driver =  new ChromeDriver();
	   
		  if(browser.equalsIgnoreCase("chrome"))
			 {
			 System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
			 ChromeOptions option=new ChromeOptions();
			 option.addArguments("----disable-notification----");
			 driver=new ChromeDriver(option);
			 driver.manage().window().maximize();
			  }
			 else if(browser.equalsIgnoreCase("firefox"))
			     {
			 System.setProperty("webdriver.gecko.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
			 FirefoxOptions option=new FirefoxOptions();
			 option.addArguments("----disable-notification----");
			 driver=new FirefoxDriver(option);
			 driver.manage().window().maximize();
			 }


			 else
			 {
			 throw new Exception("Browser is not correct");
			 }

			 driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);


	  }
	
  @Test(priority=0)
  public void Login_valid_Emailid_pswd() throws InterruptedException {
	  
	  driver.manage().window().maximize();
		driver.get("https://www.shopclues.com/");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	    Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("hiralagajjar@gmail.com");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("hiral12345");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
	  Thread.sleep(2000);
	  Actions action=new Actions(driver);
	  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  action.moveToElement(element).perform();
		
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[1]/ul/li[4]/a")).click();
	  
  }
  

  @Test(priority=1)
  public void Blank_Value() throws InterruptedException {
	  
	  
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(5000);
	  String actual_message = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]")).getText();
	  String expected_message = "Current password cannot be empty";
	  Assert.assertEquals(actual_message, expected_message);
	  Thread.sleep(5000);
		
	 
 }
  
  @Test(priority=2)
  public void Old_paswd() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("hiral12345");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
	  String expected_message1 = "New password cannot be empty";
	  Assert.assertEquals(actual_message1, expected_message1);
	  
	  Thread.sleep(5000);
		
	  
	  
  }
  
  @Test(priority=3)
  public void New_Paswd_Enter_Numeric_Value() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("hiral12345");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("1234567");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
	  String expected_message1 = "Password must be alphanumeric";
	  Assert.assertEquals(actual_message1, expected_message1);
	  
	  Thread.sleep(5000);
		
  }
  
  @Test(priority=4)
  public void New_Paswd_Check_Minimum_Char() throws InterruptedException {
	  
	  
	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("hiral12345");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("hiral");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
	  String expected_message1 = "Password must be 6 characters or more";
	  Assert.assertEquals(actual_message1, expected_message1);
	  Thread.sleep(5000);
		
  }
  
  @Test(priority=5)
  public void New_Paswd_Enter() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("hiral12345");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("hiral14");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"password2Blank\"]")).getText();
	  String expected_message1 = "Confirm password cannot be empty";
	  Assert.assertEquals(actual_message1, expected_message1);
	  
	  Thread.sleep(5000);
		
  }
  
  @Test(priority=6)
  public void Paswd_MisMatch() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("hiral12345");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("hiral14");
	  driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("hiral");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"newEqualToConfirm\"]")).getText();
	  String expected_message1 = "New password and confirm new password do not match";
	  Assert.assertEquals(actual_message1, expected_message1);
		
	  Actions action=new Actions(driver);
	  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  action.moveToElement(element).perform();
	   driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/div/ul/li[10]/a")).click();
  
  }
  @AfterTest
  public void afterTest() {
  }

}
