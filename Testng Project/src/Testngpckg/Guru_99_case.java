package Testngpckg;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Guru_99_case {
	public WebDriver driver;
	
	
	@BeforeTest
	  public void beforeTest() {
		
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
	    driver =  new ChromeDriver();
	    driver.manage().window().maximize();
		driver.get(" http://demo.guru99.com/test/newtours/");
		
		
	  }
	
	
	
  @Test
  public void f() {
	  
	  	String actualtitle;
	  	actualtitle=driver.getTitle();
	    System.out.println("Page Title is"+ actualtitle);
		
		Assert.assertTrue(actualtitle.contains("Welcome:Mercury Tours"));
		String actual_result=driver.getTitle();
		String Expected_result="Welcome:Mercury Tours";
		Assert.assertEquals(actual_result, Expected_result);
  }
  

  @AfterTest
  public void afterTest() {
  }

}
