package Testngpckg;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class DateAndTime_Guru99 {
	public WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
	    driver =  new ChromeDriver();
	    driver.manage().window().maximize();
		driver.get( "http://demo.guru99.com/test/");
		Thread.sleep(3000);
		
  }
	  
	
  @Test(priority=0)
  public void datetime_picker() throws InterruptedException {
	  
	  WebElement element=driver.findElement(By.xpath("/html/body/form/input[1]"));
	  element.sendKeys("22122020");
	  element.sendKeys(Keys.TAB);
	  element.sendKeys("15:03");
	  driver.findElement(By.xpath("/html/body/form/input[2]")).click();
	  Thread.sleep(2000);
	  
	  String actual_message = driver.findElement(By.xpath("/html/body/div[2]")).getText();
	  String expected_message = "Your Birth Date is 2020-12-22\n"
	  		+ "Your Birth Time is 15:03";
	  Assert.assertEquals(actual_message, expected_message);
		

	  
	  
  }
  

  @AfterTest
  public void afterTest() {
  }

}
