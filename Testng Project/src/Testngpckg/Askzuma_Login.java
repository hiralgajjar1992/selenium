package Testngpckg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Askzuma_Login {
	public WebDriver driver;
	
	
	@BeforeTest
	 @Parameters("browser")
	  public void beforeTest(@Optional("firefox")String browser) throws Exception {
		//System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
        //driver =  new ChromeDriver();
        
		 if(browser.equalsIgnoreCase("chrome"))
		 {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		 ChromeOptions option=new ChromeOptions();
		 option.addArguments("----disable-notification----");
		 driver=new ChromeDriver(option);
		 driver.manage().window().maximize();
		  }
		 else if(browser.equalsIgnoreCase("firefox"))
		     {
		 System.setProperty("webdriver.gecko.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		 FirefoxOptions option=new FirefoxOptions();
		 option.addArguments("----disable-notification----");
		 driver=new FirefoxDriver(option);
		 driver.manage().window().maximize();
		 }


		 else
		 {
		 throw new Exception("Browser is not correct");
		 }

		 driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

	  }
	
	
	@Test(priority=0)
  public void bothvalueblank() throws InterruptedException {
		
			driver.manage().window().maximize();
			driver.get("https://askzuma.com/");
			driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
			Thread.sleep(5000);
			
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		Thread.sleep(5000);
		
		String actual_message = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expected_message = "Required";
		Assert.assertEquals(actual_message, expected_message);
		
		String actual_message1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		String expected_message1 = "Required";
		Assert.assertEquals(actual_message1, expected_message1);

	}	
	
  
	@Test(priority=1)
	  public void emailblank() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345");
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		
		String actual_message2 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expected_message2 = "Required";
		Assert.assertEquals(actual_message2, expected_message2);
		
		
	  }
	
	
	@Test(priority=2)
	  public void passwordblank() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
		
		
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("hiralagajjar@gmail.com");
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		
		String actual_message3 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		String expected_message3 = "Required";
		Assert.assertEquals(actual_message3, expected_message3);

	  }
	
	
	@Test(priority=3)
	  public void emailinvalid() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();
		
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("hiralgajjargmail.com");
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		
		String actual_message4 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expected_message4 = "Invalid format";
		Assert.assertEquals(actual_message4, expected_message4);

		
		
	  }
	

	@Test(priority=4)
	  public void emailpaswdvalid() throws InterruptedException {
		driver.navigate().refresh();
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
		
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345678");
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		
		Thread.sleep(5000);
		
		
	  }
	
	@Test(priority=5)
	  public void Search() throws InterruptedException {
		
		
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul/li[2]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"tbLocation\"]")).sendKeys("usa");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"tbShopName\"]")).sendKeys("Euro Motocars");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"btnSearchShops\"]")).click();
		Thread.sleep(5000);
		
		
	  }
	
	
	@Test(priority=6)
	  public void LogOut() throws InterruptedException {
		
		driver.findElement(By.xpath("/html/body/div[7]/header/div/nav/a")).click();
		Thread.sleep(7000);
		driver.close();
		
		
	  }
	
	
  @AfterTest
  public void afterTest() {
	  
  }

}
