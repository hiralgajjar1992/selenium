package Testngpckg;

import org.openqa.selenium.*;


import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;

public class Shopclues_Login {
	public WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
	    driver =  new ChromeDriver();
	    driver.manage().window().maximize();
		driver.get("https://www.shopclues.com/");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	    Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		
	  }
  @Test(priority=0)
  public void Both_Value_Blank() {
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  String actual_message = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
		String expected_message = "Please enter email id or mobile number.";
		Assert.assertEquals(actual_message, expected_message);
		
		String actual_message1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
		String expected_message1 = "Please enter your password.";
		Assert.assertEquals(actual_message1, expected_message1);
	  
  }
  

  @Test(priority=1)
  public void Pswd_Blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("hiralagajjar@gmail.com");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  String actual_message2 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
		String expected_message2 = "Please enter your password.";
		Assert.assertEquals(actual_message2, expected_message2);
	  
  }
  
  @Test(priority=2)
  public void Emailid_Blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("hiral559");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  String actual_message3 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
		String expected_message3 = "Please enter email id or mobile number.";
		Assert.assertEquals(actual_message3, expected_message3);
		
	  
  }
  
  @Test(priority=3)
  public void InValid_Emailid() throws InterruptedException {
	  
	  
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("hiralagajjargmail.com");
	  Thread.sleep(3000);
	  
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  String actual_message4 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
		String expected_message4 = "Please enter valid email id or mobile number.";
		Assert.assertEquals(actual_message4, expected_message4);
		
  }
  
  
  
  @Test(priority=4)
  public void InVaild_paswd() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("12345");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  String actual_message5 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
		String expected_message5 = "Password must be 6 characters or more.";
		Assert.assertEquals(actual_message5, expected_message5);
  }
  
  @Test(priority=5)
  public void Vaild_Emilid_paswd() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814+a4@gmail.com");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu8144");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
	  
	  Actions action=new Actions(driver);
	  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  action.moveToElement(element).perform();
	  
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[10]/a")).click();
	  
  }
  
  @AfterTest
  public void afterTest() {
  }
}
