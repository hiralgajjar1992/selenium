package Testngpckg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Askzuma_home_page {
	public WebDriver driver;
	
	
	@BeforeTest
	@Parameters("browser")
	  public void beforeTes(@Optional("firefox")String browser) throws Exception {
	
		//System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
	    //driver =  new ChromeDriver();
	    
	    if(browser.equalsIgnoreCase("chrome"))
		 {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		 ChromeOptions option=new ChromeOptions();
		 option.addArguments("----disable-notification----");
		 driver=new ChromeDriver(option);
		 driver.manage().window().maximize();
		  }
		 else if(browser.equalsIgnoreCase("firefox"))
		     {
		 System.setProperty("webdriver.gecko.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		 FirefoxOptions option=new FirefoxOptions();
		 option.addArguments("----disable-notification----");
		 driver=new FirefoxDriver(option);
		 driver.manage().window().maximize();
		 }


		 else
		 {
		 throw new Exception("Browser is not correct");
		 }

		 driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

	    
	  }
	
	
	
  @Test
  public void DropDown() throws InterruptedException {
	  driver.manage().window().maximize();
		driver.get("https://askzuma.com/");
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0, 700)");
		Thread.sleep(2000);
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"select2-chosen-1\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"select2-result-label-8\"]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"select2-chosen-46\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"select2-result-label-47\"]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"select2-chosen-90\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"select2-result-label-91\"]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"select2-chosen-93\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"select2-result-label-94\"]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.id("selectJob")).click();
		Thread.sleep(5000);
		driver.close();
		
//		String actualresult=driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
//		String expected_result="Please specify the the location.";
//		Assert.assertEquals(actualresult, expected_result);
//		Thread.sleep(5000);
		
  }
  

  @AfterTest
  public void afterTest() {
  }

}
