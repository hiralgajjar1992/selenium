package Testngpckg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Edit_Profile {
	public WebDriver driver;
	
	@BeforeTest
	 @Parameters("browser")
	  public void beforeTest(@Optional("firefox")String browser) throws Exception {
		

		 if(browser.equalsIgnoreCase("chrome"))
		 {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		 ChromeOptions option=new ChromeOptions();
		 option.addArguments("----disable-notification----");
		 driver=new ChromeDriver(option);
		 driver.manage().window().maximize();
		  }
		 else if(browser.equalsIgnoreCase("firefox"))
		     {
		 System.setProperty("webdriver.gecko.driver", "C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		 FirefoxOptions option=new FirefoxOptions();
		 option.addArguments("----disable-notification----");
		 driver=new FirefoxDriver(option);
		 driver.manage().window().maximize();
		 }


		 else
		 {
		 throw new Exception("Browser is not correct");
		 }

		 driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

		
	  }
	
  @Test(priority=0)
  public void Login_valid_Email_Paswd() throws InterruptedException {
	  driver.get("https://www.shopclues.com/");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	    Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("hiralagajjar@gmail.com");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("hiral12345");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
	  Thread.sleep(2000);
	  Actions action=new Actions(driver);
	  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  action.moveToElement(element).perform();
		
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a/i")).click();
	  Thread.sleep(2000);
  }
  
  @Test(priority=1)
  public void Clear_data () throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]")).getText();
	  String expected_message = "First name cannot be blank";
	  Assert.assertEquals(actual_message, expected_message);
  }
  
  @Test(priority=2)
  public void First_Name_Enter() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("hiral123");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]")).getText();
	  String expected_message1 = "Last name cannot be blank";
	  Assert.assertEquals(actual_message1, expected_message1);
	  Thread.sleep(2000);
	  
	  
	  
  }
  
  @Test(priority=3)
  public void Last_Name_Enter() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("gajjar");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(2000);
	  
	  String actual_message2 = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]")).getText();
	  String expected_message2 = "Mobile number cannot be blank";
	  Assert.assertEquals(actual_message2, expected_message2);
	  Thread.sleep(5000);
	  
	  
  }
  
  @Test(priority=4)
  public void Enter_Mobile_Number() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("8735952476");
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(5000);
	  
	  String actual_message3 = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]")).getText();
	  String expected_message3 = "First name cannot be blank";
	  Assert.assertEquals(actual_message3, expected_message3);
	  Thread.sleep(2000);

  }

  
  @Test(priority=5)
  public void Invalid_Data_Enter() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("#^$^$%&&^*%^*^%&%$%");
	  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("4845615864894189489564");
	  driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("bugfbgiufjbgiufgjbfgub");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(5000);
	  
	  String actual_message4 = driver.findElement(By.xpath("//*[@id=\"fname\"]")).getText();
	  String expected_message4= "Enter correct first name";
	  Assert.assertEquals(actual_message4, expected_message4);
	  Thread.sleep(5000);
	  
  } 
	  

	  @Test(priority=6)
	  public void Valid_Data_Enter() throws InterruptedException {
		  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
		  driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Hiral");
		  driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("Gajjar");
		  driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("8735952476");
		  Thread.sleep(5000);
		  
		  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		  Thread.sleep(2000);
		  
		  
		  Actions action=new Actions(driver);
		  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		  action.moveToElement(element).perform();
		  
		   driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/div/ul/li[10]/a")).click();
	  
}@AfterTest
  public void afterTest() {
	  
  }

}
