package Testngpckg;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shopclues_Reg {
public WebDriver driver;

@BeforeTest
public void beforeTest() throws InterruptedException {
	System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
    driver =  new ChromeDriver();
    driver.manage().window().maximize();
	driver.get("https://www.shopclues.com/");
	Thread.sleep(10000);
	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
	
}

  @Test(priority=0)
  public void Both_value_Blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"reg_tab\"]")).click();
	  
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  Thread.sleep(5000);
	  
	  String actual_message = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
		String expected_message = "Please enter your email id.";
		Assert.assertEquals(actual_message, expected_message);
		
		String actual_message1 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
		String expected_message1 = "Please enter your mobile number.";
		Assert.assertEquals(actual_message1, expected_message1);
  }
  

  @Test(priority=1)
  public void Email_Blank() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("8735952476");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  
	  String actual_message2 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
		String expected_message2 = "Please enter your email id.";
		Assert.assertEquals(actual_message2, expected_message2);
	  
	  
  }

  @Test(priority=2)
  public void Mobile_Number_Blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("hiralagajjar@gmail.com");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  
	  String actual_message3 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
		String expected_message3 = "Please enter your mobile number.";
		Assert.assertEquals(actual_message3, expected_message3);
  }

  @Test(priority=3)
  public void Invalid_Email_MobileNumber() throws InterruptedException  {
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).clear();
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).clear();
	  
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("hiralagajjargmail.com");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("873595");
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  
  }

  @AfterTest
  public void afterTest() {
  }

}


